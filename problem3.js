const { error } = require('console')
const https = require('https')
const apikey = 'de0348c9ef2dab76eb702fd8ba4d4be6'
const apitoken = 'ATTA2f760e1f8f31ec1122c2e11e7beb0aefcf9bf8eb4e70d00c2543f6c64e250460E8885D9E'

function getLists(boardId) {
    const url =`https://api.trello.com/1/boards/${boardId}/lists?key=${apikey}&token=${apitoken}`
    return new Promise((resolve,reject)=>{
        const request = https.get(url,(response)=>{
            let data =''
            response.on('data',(chunk)=>{
                data +=chunk
            })
            response.on('end',()=>{
                resolve(JSON.parse(data))
            })
        })
        request.end(error,()=>{
            reject(error)
        })
    })
}
module.exports = getLists