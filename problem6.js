const https = require('https')
const key = 'de0348c9ef2dab76eb702fd8ba4d4be6'
const token = 'ATTA2f760e1f8f31ec1122c2e11e7beb0aefcf9bf8eb4e70d00c2543f6c64e250460E8885D9E'
const option = { method: 'POST' }
function createList(boardId, listName) {
    const url = `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${key}&token=${token}`
    return new Promise((resolve, reject) => {
        const request = https.request(url, option, (response) => {
            response.setEncoding('utf8')
            response.on('data', (chunk) => {
                resolve(JSON.parse(chunk))
            })
            response.on('end', () => {
            })
        })
        request.on('error', (error) => {
            reject(error)
        })
        request.write(JSON.stringify(option))
        request.end()
    })
}

function createCard(listId) {
    const url = `https://api.trello.com/1/cards?idList=${listId}&key=${key}&token=${token}`
    return new Promise((resolve, reject) => {
        const request = https.request(url, option, (response) => {
            response.setEncoding('utf8')
            response.on('data', (chunk) => {
                resolve(JSON.parse(chunk))
            })
            response.on('end', () => {
            })
        })
        request.on('error', (error) => {
            reject(error)
        })
        request.write(JSON.stringify(option))
        request.end()
    })
}
module.exports = {createList,createCard}