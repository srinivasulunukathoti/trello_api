const https = require('https')
const key ='de0348c9ef2dab76eb702fd8ba4d4be6'
const token = 'ATTA2f760e1f8f31ec1122c2e11e7beb0aefcf9bf8eb4e70d00c2543f6c64e250460E8885D9E'
const option = {method:'PUT'}
function deleteAllListCreated(id) {
    const url = `https://api.trello.com/1/lists/${id}/closed?key=${key}&token=${token}&value=true`
    return new Promise((resolve,reject)=>{
        const request = https.request(url,option,(response)=>{
            let data = ''
            response.setEncoding('utf8')
            response.on('data',(chunk)=>{
                data +=chunk
            })
            response.on('end',()=>{
                resolve(data)
            })
        })
        request.on('error',(error)=>{
            reject(error)
        })
        request.end()
    })
}
module.exports = deleteAllListCreated