const {getCheckItems ,updateStatus} = require('../problem10')
const getAllCards = require('../problem5')

getAllCards('aSzmNgJE').then((cards)=>{
    // console.log(cards);
        let data = []
        cards.flat().forEach((card)=>{
            // console.log( card.id)
             data.push(getCheckItems(card.id))
        })
  return Promise.all(data)
}).then((data)=>{
    // console.log(data);
    let promises = []
    let checkListData = data.map((lists)=>{
        return JSON.parse(lists)
    })
  
    checkListData.flat().forEach(checklist => {
        // console.log(checklist);
        checklist.checkItems.forEach((checkItem)=>{
            // console.log(checkItem);
            setTimeout(() => {
                updateStatus(checklist.idCard,checkItem.id).then(()=>{
                    console.log('updated')
                }).catch((error)=>{
                    console.log(error);
                })
            }, 3000);
            
        })

    });
}).catch((error)=>{
    console.log(error );
})



