const createBoard = require('../problem2')
const {createList,createCard} = require('../problem6')
let listName = ["ramaChandra","banu","balaji"]
 
createBoard('Sri').then((data)=>{
    let result = JSON.parse(data)
    const lists = []
    for (let index = 0; index < listName.length; index++) {
        lists.push(createList(result.id ,listName[index]))
    }
    return Promise.all(lists)
}).then((values)=>{
    let cardsData = []
    values.forEach((value)=>{
        cardsData.push(createCard(value.id))
    })
    return Promise.all(cardsData)
}).then((cards)=>{
    console.log(cards);
})