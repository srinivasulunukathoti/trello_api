const getAllCards = require('../problem5')
const {getCheckItems,updateStatus} = require('../problem9')

getAllCards('aSzmNgJE').then((cards)=>{
    // console.log(cards);
        let data = []
        cards.flat().forEach((card)=>{
            // console.log( card.id)
             data.push(getCheckItems(card.id))
        })
  return Promise.all(data)
}).then((data)=>{
    // console.log(data);
    let promises = []
    let checkListData = data.map((lists)=>{
        return JSON.parse(lists)
    })
  
    checkListData.flat().forEach(checklist => {
        // console.log(checklist);
        checklist.checkItems.forEach((checkItem)=>{
            console.log(checkItem);
            console.log();
            promises.push(updateStatus(checklist.idCard,checkItem.id))
        })

    });
    return Promise.all(promises)
}).then((data)=>{
    console.log(data)
}).catch((error)=>{
    console.log(error);
})
