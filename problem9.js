const https = require('https')
const key ='de0348c9ef2dab76eb702fd8ba4d4be6'
const token = 'ATTA2f760e1f8f31ec1122c2e11e7beb0aefcf9bf8eb4e70d00c2543f6c64e250460E8885D9E'
function getCheckItems(id) {

    return fetch(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`,{
     method: 'GET'
   })
     .then(response => {
       console.log(
         `Response: ${response.status} ${response.statusText}`
       );
       return response.text();
     })
   }
   function updateStatus(id, idCheckItem){
    return fetch(`https://api.trello.com/1/cards/${id}/checkItem/${idCheckItem}?key=${key}&token=${token}&state=complete`, {
    method: 'PUT'
  })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.text();
    })
  }
   
module.exports = {getCheckItems,updateStatus}