const https = require('https')
const getLists = require('./problem3')
const getCards = require('./problem4')

function getAllCards(boardId) {
    return new Promise((resolve, reject) => {
        getLists(boardId)
            .then((lists) => {
                const listIds = lists.map((list) => {
                    return list.id
                })
                const promises = listIds.map((id)=>{return getCards(id)})
                    Promise.all(promises)
                        .then((allCards) => {
                            resolve(allCards)
                        })
                        .catch((error) => {
                            reject(error)
                        })
                })

            })
    }
module.exports = getAllCards
